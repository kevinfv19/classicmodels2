package fp.daw.classicmodels;


import java.util.Properties;
import java.util.Date;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;



public class MailService {
	public static void sendMessage(String smtpServer, String port, String account, String password,
			String to, String subject, String body, String type) throws MessagingException {
			Properties properties = new Properties();
			// Servidor SMTP.
			properties.setProperty("mail.smtp.host", smtpServer);
			// Habilitar TLS.
			properties.setProperty("mail.smtp.starttls.enable", "true");
			// Puerto para envo de correos.
			properties.setProperty("mail.smtp.port", port);
			// Si requiere usuario y password para conectarse.
		//	if (user != null && password != null)
			properties.setProperty("mail.smtp.auth", "true");
			// Obtener un objeto Session con las propiedades establecidas.
			Session mailSession = Session.getInstance(properties);
			// Para obtener un log de salida ms extenso.
			mailSession.setDebug(true);
			Message message = new MimeMessage(mailSession);
			message.setFrom(new InternetAddress(account));
			message.addRecipient(Message.RecipientType.TO, new InternetAddress(to));
			message.setSubject(subject);
			message.setContent(body, type);
			message.setSentDate(new Date());
			Transport.send(message, account, password);
}
	/**
	 * 
	 * @param email
	 * @param confirmationCode
	 * @throws MessagingException 
	 */
	public static void sendConfirmationMessage(String email, String confirmationCode) throws MessagingException{
		StringBuilder body=new StringBuilder();
		body.append("<a href=\"http://localhost?cc=");
		body.append(confirmationCode);
		body.append("\">Confirmar</a>");
		sendMessage(
				"smtp.gmail.com",//servidor SMPT
				"587",//puerto
				"classicmodelsfp@gmail.com",//cuenta para iniciar sesion
				"FP2021cifp",//conttrasea
				email,//direccion del destinatario
				"Confirma tu direccion de correo electronico",
				body.toString(),
				"text/html"
				);
	}


}
