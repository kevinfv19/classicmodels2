<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    
<c:if test="${empty sessionScope.customer}">
	<c:redirect url="catalogo.jsp" />
</c:if>

<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>Carrito</title>
</head>
<body>
	<h1>Carrito de la compra</h1>
	<p><a href="catalogo.jsp">Catálogo</a></p>
	<c:choose>
		<c:when test="${empty sessionScope.cart}">
			<p>No se han añadido productos al carrito</p>
		</c:when>
		<c:otherwise>
			<table>
				<tr><th>Producto</th><th>Unidades</th>
				<th>Precio</th><th>Importe</th></tr>
				<c:forEach var="linea" items="${sessionScope.cart}">
					<tr>
						<td>${linea.value.productName}</td>
						<td>${linea.value.amount}</td>
						<td>${linea.value.price}</td>
						<td>${linea.value.amount * linea.value.price}</td>
					</tr>
				</c:forEach>
			</table>
		</c:otherwise>		
	</c:choose>
</body>
</html>
