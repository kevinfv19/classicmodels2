<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8" session="true"%>
<%
if (session.getAttribute("user") != null)
	response.sendRedirect("catalogo");
else {
	String firstName = request.getParameter("nombre");
	String lastName = request.getParameter("apellidos");
	String email = request.getParameter("email");
	if (firstName == null || lastName == null || email == null)
		response.sendRedirect("catalogo");
	else {
%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Registro</title>
</head>
<%
System.out.println("hola");
%>
<body>
	<h1>
		Bienvenido
		<%=firstName%></h1>
	<p>
		Sólo queda un paso más para completar tu registro y tener acceso a
		todos nuestros Servicios, sigue las instrucciones que te hemos enviado
		a
		<%=email%></p>
	<p>Pulsa el botón de reenviar para que te volvamos a enviar el
		mensaje de confirmación:</p>
	<form action="reenvio" method="post">
		<input type="hidden" name="email" value="<%=email%>" />
		<p>
			<input type="submit" value="Reenviar" />
		</p>
		
	</form>

	<p>
		<a href="login.jsp">Iniciar sesión</a>
	</p>
	<p>
		<a href="catalogo.jsp">Página de inicio</a>
	</p>
</body>
</html>
<%
}
}
%>