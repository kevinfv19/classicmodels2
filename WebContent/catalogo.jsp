<%@ taglib uri="http://java.sun.com/jsp/jstl/sql" prefix="sql" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    
<sql:query var="modelos" dataSource="jdbc/classicmodels">
    select productCode, productName, productScale, productDescription, MSRP from products;
</sql:query>


<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
<title>Catálogo de Productos</title>

 <style>



</style>

</head>
<body>

<div class="container-fluid">

	
	<h1>Catálogo de Modelos a Escala</h1>
	<h4>Kevin Fernández Vázquez</h4>
	
</div>

<hr/>

<ul class="nav justify-content-left ml-3 mb-4">

<c:choose>

	<c:when test="${not empty sessionScope.customer }">
	
		<li class="nav-item">
	
			${sessionScope.customer }
		
			<a href="cart.jsp">Carrito</a>
		
			<a href="logout.jsp" >Cerrar Sesión</a>
			
		</li>

	</c:when>
	<c:otherwise>
	
		<li class="nav-item">
	
			<a href="login.jsp" >Iniciar Sesión</a>
			
			<a href="registro.jsp" >Registrarse</a>
			
		</li>

	</c:otherwise>

</c:choose>

</ul>
	
<div class="container-fluid">
	<c:forEach var="modelo" items="${modelos.rows}">
	
		<details >
	
			<summary class="btn btn-secondary mb-2"><c:out value="${modelo.productName}" /></summary>
			<br>
			
			<p>
				Escala
				<c:out value="${modelo.productScale}" />
			</p>
			<p>
				<c:out value="${modelo.productDescription}" />
			</p>
			
			<p>
			Precio: 
			<c:out value="${modelo.MSRP}" />
			
			<c:if test="${not empty sessionScope.customer}">
				<a href="cart?c=${modelo.productCode}&n=${modelo.productName}&p=${modelo.MSRP}">
					<img style="width: 1em; height: 1em;" src="http://simpleicon.com/wp-content/uploads/cart-plus.svg" />
				</a>
			</c:if>
			</p>
		
		</details>
		
		<hr />
		
	</c:forEach>
	
</div>

</body>
</html>

