<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1" session="true"%>
 <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<c:if test="${not empty sessionScope.customer}">
	<c:redirect url="catalogo.jsp"/>
</c:if>

<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Inicio de Sesi�n</title>
</head>
<body>
	<form action="auth" method="post">
		<p>
			<label for="email">eMail</label>
		</p>
		<p>
			<input type="email" name="email" required="required" value="diegofreyre@mail.com" />
		</p>
		<p>
			<label for="password">Contrase�a</label>
		</p>
		<p>
			<input type="password" name="password" required="required" value="practicas" />
		</p>
		<p>
			<input type="submit" value="Login" />
		</p>
	</form>
	<form action="catalogo.jsp">
		<input type="submit" value="Volver" />
	</form>
	<c:choose>
		<c:when test="${param.status == 1}">
			<p>Las credenciales del usuario no son correctas</p>
		</c:when>
		<c:when test="${param.status == 2}">
			<p>Error, del sistema, ponte en contacto con el administrador</p>
		</c:when>
	</c:choose>
</body>
</html>
