<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8" session="true"%>

<c:if test="${not empty sessionScope.customer}">
	<c:redirect url="catalogo.jsp" />
</c:if>


<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Registro de Usuarios</title>
<script>
	/*
	 * No deja hacer si las contraseñas no coinciden
	 */
	function check(confirm) {
		if (confirm.value != document.getElementById('password').value) {
			confirm.setCustomValidity('Las contraseñas no coinciden');
		} else
			confirm.setCustomValidity('');
	}
</script>
</head>
<body>
	<form action="signup" method="post" onsubmit="check()">
		<h1>Registro de usuario</h1>
		<hr />
		<p>
			<label for="nombre">Nombre</label>
		</p>
		<p>
			<input type="text" name="nombre" required="required" />
		</p>
		<p>
			<label for="apellidos">Apellidos</label>
		</p>
		<p>
			<input type="text" name="apellidos" required="required" />
		</p>
		<p>
			<label for="email">Email</label>
		</p>
		<p>
			<input type="email" name="email" required="required" />
		</p>
		<p>
			<label for="password">Contraseña</label>
		</p>
		<p>
			<input type="password" name="password" id="password"
				required="required" />
		</p>
		<p>
			<label for="password">Confirmar contraseña</label>
		</p>
		<p>
			<input type="password" id="confirm" required="required"
				oninput="check(this)" />
		</p>
		<p>
			<input type="submit" value="Login" />
		</p>

	</form>
	<form action="catalogo.jsp">
		<input type="submit" value="Volver" />
	</form>

</body>
</html>

